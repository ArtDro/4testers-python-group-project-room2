from src.plane import Airplane


def test_initial_state_of_plane():
    airplane = Airplane('Boeing 737', 350)
    assert airplane.mileage == 0
    assert airplane.seats_occupied == 0


def test_fly_mileage_once():
    airplane = Airplane('Boeing 737', 350)
    airplane.fly(5000)
    assert airplane.mileage == 5000


def test_fly_mileage_twice():
    airplane = Airplane('Boeing 737', 350)
    airplane.fly(5000)
    airplane.fly(3000)
    assert airplane.mileage == 8000


def test_if_service_is_required():
    airplane = Airplane('Boeing 737', 350)
    airplane.fly(9999)
    assert airplane.is_service_required()
    airplane.fly(10001)
    assert airplane.is_service_required()


def test_available_seats_once():
    airplane = Airplane('Boeing 737', 200)
    airplane.seats = 200
    airplane.seats_occupied = 180
    assert airplane.seats - airplane.seats_occupied == 20


def test_available_seats():
    airplane = Airplane('Boeing 737', 200)
    airplane.board_passengers(201)
    assert airplane.get_available_seats() <= 0
    assert airplane.seats_occupied == 200

