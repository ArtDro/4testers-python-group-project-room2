
class Airplane:
    def __init__(self, name, seats):
        self.name = name
        self.seats = seats
        self.mileage = 0
        self.seats_occupied = 0

    def fly(self, distance):
        self.mileage += distance

    def is_service_required(self):
        if self.mileage > 10_000:
            return True
        else:
            return f'masz jescze czas'

    def board_passengers(self, number_of_passengers):
        self.seats_occupied += number_of_passengers
        if number_of_passengers > self.seats:
            return 0
        else:



    def get_available_seats(self):
        return self.seats - self.seats_occupied


if __name__ == '__main__':

    airplane1 = Airplane('Boeing 737', 350)
    airplane2 = Airplane('Antonov 225', 420)
    print(airplane1.mileage)
    airplane1.fly(3000)
    print(airplane1.mileage)
    airplane2.fly(4500)
    print(airplane2.mileage)
    print(airplane1.is_service_required())
    print(airplane2.is_service_required())
    airplane1.board_passengers(250)
    airplane2.board_passengers(310)
    print(airplane1.seats_occupied)
    print(airplane2.seats_occupied)
    print(airplane1.get_available_seats())
    print(airplane2.get_available_seats())
